package si.mitic.diploma.proxy;

import cloud.commandframework.execution.CommandExecutionCoordinator;
import cloud.commandframework.velocity.VelocityCommandManager;
import cloud.commandframework.velocity.arguments.PlayerArgument;
import com.google.inject.Inject;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.player.KickedFromServerEvent;
import com.velocitypowered.api.event.player.PlayerChooseInitialServerEvent;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.plugin.PluginContainer;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.ProxyServer;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import com.velocitypowered.api.scheduler.ScheduledTask;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;
import org.redisson.api.RemoteInvocationOptions;
import org.slf4j.Logger;
import si.mitic.diploma.common.Redis;
import si.mitic.diploma.common.RedisWorldManager;
import si.mitic.diploma.common.data.*;
import si.mitic.diploma.common.interfaces.NodeService;

import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;


@Plugin(id = "proxy-plugin", name = "Proxy Plugin", version = "1.0", authors = {"Miha Mitič"})
public class ProxyPlugin {

    private final ProxyServer server;
    private final Logger logger;
    private final Redis redis;
    private final ServerData serverData;
    private final HashMap<String, NodeService> nodeServices;
    private final HashMap<UUID, ScheduledTask> tasks;
    private VelocityCommandManager<CommandSource> velocityCommandManager;
    private PluginContainer pluginContainer;
    private final RedisWorldManager worldManager;

    @Inject
    public ProxyPlugin(ProxyServer server, Logger logger) {
        this.server = server;
        this.logger = logger;
        this.redis = new Redis(Redis.getConfigFromEnv());
        this.serverData = ServerData.getPodInfoFromEnv(ServerType.PROXY);
        this.nodeServices = new HashMap<>();
        this.tasks = new HashMap<>();
        this.worldManager = redis.getWorldManager();

        logger.info("Pod info: {}", serverData);
    }

    @Subscribe
    public void onProxyInitialization(ProxyInitializeEvent event) {
        pluginContainer = server.getPluginManager().fromInstance(this).orElseThrow();
        server.getScheduler().buildTask(this, this::syncServers).repeat(Duration.ofSeconds(1)).schedule();

        velocityCommandManager = new VelocityCommandManager<>(
                pluginContainer,
                server,
                CommandExecutionCoordinator.simpleCoordinator(),
                Function.identity(),
                Function.identity()
        );

        velocityCommandManager.command(
                velocityCommandManager.commandBuilder("dump")
                        .handler(handler -> {
                            for (Map.Entry<String, ServerInfo> e : redis.getServers().readAllEntrySet()) {
                                logger.info("{} - {}", e.getKey(), e.getValue());
                            }
                        })
        );

        velocityCommandManager.command(
                velocityCommandManager.commandBuilder("hub")
                        .senderType(Player.class)
                        .handler(handler -> {
                            Player p = (Player) handler.getSender();
                            Optional<RegisteredServer> lobby = getLobby();
                            if (lobby.isEmpty()) {
                                p.sendMessage(Component.text("No lobby found!", NamedTextColor.RED));
                                return;
                            }
                            p.sendMessage(Component.text("Sending you to " + lobby.get().getServerInfo().getName() + "!", NamedTextColor.GREEN));
                            p.createConnectionRequest(lobby.get()).fireAndForget();
                        })
        );
        velocityCommandManager.command(
                velocityCommandManager.commandBuilder("play")
                        .handler(handler -> {
                            CommandSource sender = handler.getSender();
                            if (!(sender instanceof Player p)) return;

                            UUID uuid = p.getUniqueId();
                            startWorldRequest(p, uuid);
                        })
        );
        velocityCommandManager.command(
                velocityCommandManager.commandBuilder("visit")
                        .argument(PlayerArgument.of("player"))
                        .handler(handler -> {
                            CommandSource sender = handler.getSender();
                            if (!(sender instanceof Player p)) return;
                            Player player = handler.get("player");
                            /*String username = handler.get("username");

                            Optional<Player> player = server.getPlayer(username); // TODO Support previous players
                            if(player.isEmpty()){
                                p.sendMessage(Component.text("Player not found!", NamedTextColor.RED));
                                return;
                            }*/

                            startWorldRequest(p, player/*.get()*/.getUniqueId());
                        })
        );
    }

    private void startWorldRequest(Player p, UUID uuid) {
        WorldInfo worldInfo = worldManager.getWorldInfo(uuid);
        if (worldInfo.isUnassigned()) {
            boolean success = prepareWorld(p, uuid);
            if (!success) return;
            connectToWorldWhenLoaded(p, uuid);
        } else {
            switch (worldInfo.getStatus()) {
                case UNASSIGNED -> {
                }
                case LOADING -> connectToWorldWhenLoaded(p, uuid);
                case READY -> sendToWorld(p, uuid);
                case SAVING -> p.sendMessage(Component.text("The world is currently being saved, please try again later!"));
            }
        }
    }

    private boolean prepareWorld(Player p, UUID uuid) {
        Optional<RegisteredServer> node = getFreeNode();
        if (node.isEmpty()) {
            p.sendMessage(Component.text("Could not find a free node server, please try again later!"));
            return false;
        }

        String name = node.get().getServerInfo().getName();
        NodeService nodeService = nodeServices.get(name);

        if (nodeService == null) {
            p.sendMessage(Component.text("Node service for " + name + " not found!", NamedTextColor.RED));
            return false;
        }

        boolean success = nodeService.prepareWorld(p.getUniqueId(), uuid);
        if (!success) {
            p.sendMessage(Component.text("Error loading world! Maybe it does not exist?", NamedTextColor.RED));
            return false;
        }

        p.sendMessage(Component.text("Loading the world on " + name + "!", NamedTextColor.GREEN));
        return true;
    }

    private void connectToWorldWhenLoaded(Player p, UUID uuid) {
        p.sendMessage(Component.text("Waiting for the world to load!"));

        ScheduledTask previousTask = tasks.put(uuid, server.getScheduler().buildTask(this, () -> {
            WorldInfo info = redis.getWorldManager().getWorldInfo(uuid);
            logger.info("Checking status {}: {}", uuid, info);
            switch (info.getStatus()) {
                case UNASSIGNED, SAVING -> {
                    p.sendMessage(Component.text("The world was not loaded, try again!"));
                    tasks.get(uuid).cancel();
                }
                case LOADING -> {
                }
                case READY -> {
                    sendToWorld(p, uuid);
                    tasks.get(uuid).cancel();
                }
            }
        }).repeat(1, TimeUnit.SECONDS).delay(1, TimeUnit.SECONDS).schedule());
        if (previousTask != null) previousTask.cancel();
    }

    private void sendToWorld(Player p, UUID uuid) {
        String nodeName = worldManager.getWorldInfo(uuid).getPodOwnerName();
        Optional<RegisteredServer> nodeOpt = server.getServer(nodeName);
        if (nodeOpt.isEmpty()) {
            p.sendMessage(Component.text("Could not find the node that the world is on! Weird!"));
            return;
        }
        NodeService nodeService = nodeServices.get(nodeName);

        if (nodeService == null) {
            p.sendMessage(Component.text("Node service for " + nodeName + " not found!", NamedTextColor.RED));
            return;
        }

        nodeService.setPlayerVisiting(p.getUniqueId(), uuid);

        p.sendMessage(Component.text("Connecting you to " + nodeOpt.get().getServerInfo().getName() + "!"));
        p.createConnectionRequest(nodeOpt.get()).fireAndForget();

    }

    @Subscribe
    public void onInitialServer(PlayerChooseInitialServerEvent event) {
        Optional<RegisteredServer> lobby = getLobby();
        logger.info("Sending {} to initial lobby {}", event.getPlayer().getUsername(), lobby.map(l -> l.getServerInfo().getName()).orElse("-"));
        event.setInitialServer(lobby.orElse(null));
    }

    @Subscribe
    public void onKick(KickedFromServerEvent event) {
        RegisteredServer target = event.getServer();
        String targetName = target.getServerInfo().getName();
        Component kickComponent = event.getServerKickReason().orElse(Component.text("Unknown"));
        logger.info("{} kicked from {}: {}", event.getPlayer().getUsername(), targetName, PlainTextComponentSerializer.plainText().serialize(kickComponent));
        if (event.kickedDuringServerConnect()) {
            Component msg = Component.text("Kicked when connecting to " + targetName + ": ", NamedTextColor.RED)
                    .append(kickComponent.color(NamedTextColor.GOLD));
            event.setResult(KickedFromServerEvent.Notify.create(msg));
            return;
        }
        Optional<RegisteredServer> lobby = getLobby();
        String lobbyName = lobby.isEmpty() ? "None" : lobby.get().getServerInfo().getName();
        logger.info("Lobby found: {}", lobbyName);
        if (lobby.isEmpty() || target.equals(lobby.get())) {
            Component msg = Component.text("No suitable lobby when redirecting! You were kicked from " + targetName + ": ", NamedTextColor.RED)
                    .append(kickComponent.color(NamedTextColor.GOLD));
            event.setResult(KickedFromServerEvent.DisconnectPlayer.create(msg));
        } else {
            Component msg = Component.text("Redirecting to lobby " + lobbyName + "! You were kicked from " + targetName + ": ", NamedTextColor.RED)
                    .append(kickComponent.color(NamedTextColor.GOLD));
            event.setResult(KickedFromServerEvent.RedirectPlayer.create(lobby.get(), msg));
        }
    }

    private Optional<RegisteredServer> getLobby() {
        return redis.getServers().values().stream()
                .filter(i -> i.getType().equals(ServerType.LOBBY))
                .min(Comparator.comparing(ServerInfo::getPlayerCount))
                .flatMap(i -> server.getServer(i.getName()));
    }

    private Optional<RegisteredServer> getFreeNode() {
        return redis.getServers().values().stream()
                .filter(i -> i.getType().equals(ServerType.NODE))
                .filter(i -> i.getWorldCount() < i.getData().getMaxWorlds())
                .min(Comparator.comparing(ServerInfo::getWorldCount))
                .flatMap(i -> server.getServer(i.getName()));
    }

    private void syncServers() {
        redis.getServers().put(serverData.getName(), new ServerInfo(serverData, ServerStatus.RUNNING, server.getPlayerCount(), Collections.emptyList()), 5, TimeUnit.SECONDS);

        redis.getServers().readAllMapAsync().thenAccept(map -> {
            for (ServerInfo info : map.values()) {
                if (!info.getType().isServer()) continue;

                if (server.getServer(info.getName()).isPresent()) continue;
                logger.info("Registering server {}!", info.getName());
                server.registerServer(new com.velocitypowered.api.proxy.server.ServerInfo(info.getName(), new InetSocketAddress(info.getIp(), 25565)));
                if (info instanceof NodeInfo) {
                    nodeServices.put(info.getName(), redis.getService(info.getName()).get(NodeService.class, RemoteInvocationOptions.defaults().expectAckWithin(10, TimeUnit.SECONDS)));
                }
            }
            for (RegisteredServer s : server.getAllServers()) {
                if (!map.containsKey(s.getServerInfo().getName())) {
                    logger.info("Unregistering server {}!", s.getServerInfo().getName());
                    server.unregisterServer(s.getServerInfo());
                    nodeServices.remove(s.getServerInfo().getName());
                }
            }
        });
    }

}
