package si.mitic.diploma.common.data;

import java.io.Serializable;
import java.util.Objects;

public class WorldInfo implements Serializable {

    public static final WorldInfo DEFAULT = new WorldInfo();

    private WorldStatus status;
    private String podOwnerName;

    public WorldInfo() {
        this.status = WorldStatus.UNASSIGNED;
        this.podOwnerName = null;
    }

    public WorldInfo(WorldStatus status, String podOwnerName) {
        this.status = status;
        this.podOwnerName = podOwnerName;
    }

    public boolean isUnassigned() {
        return status == WorldStatus.UNASSIGNED;
    }

    public WorldStatus getStatus() {
        return status;
    }

    public void setStatus(WorldStatus status) {
        this.status = status;
    }

    public String getPodOwnerName() {
        return podOwnerName;
    }

    public void setPodOwnerName(String podOwnerName) {
        this.podOwnerName = podOwnerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WorldInfo worldInfo)) return false;

        if (status != worldInfo.status) return false;
        return Objects.equals(podOwnerName, worldInfo.podOwnerName);
    }

    @Override
    public int hashCode() {
        int result = status != null ? status.hashCode() : 0;
        result = 31 * result + (podOwnerName != null ? podOwnerName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WorldInfo{" +
                "status=" + status +
                ", podOwnerName='" + podOwnerName + '\'' +
                '}';
    }
}
