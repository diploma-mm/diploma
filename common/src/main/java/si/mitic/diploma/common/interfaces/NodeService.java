package si.mitic.diploma.common.interfaces;

import java.util.UUID;

public interface NodeService extends ServerService {

    boolean prepareWorld(UUID player, UUID world);

    void setPlayerVisiting(UUID player, UUID world);

}
