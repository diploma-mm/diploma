package si.mitic.diploma.common.data;

public enum WorldStatus {

    UNASSIGNED,
    LOADING,
    READY,
    SAVING

}
