package si.mitic.diploma.common.data;

import java.io.Serializable;

public class ChatMessageInfo implements Serializable {

    private String node;
    private String sender;
    private String msg;

    public ChatMessageInfo() {
    }

    public ChatMessageInfo(String node, String sender, String msg) {
        this.node = node;
        this.sender = sender;
        this.msg = msg;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChatMessageInfo that)) return false;

        if (!node.equals(that.node)) return false;
        if (!sender.equals(that.sender)) return false;
        return msg.equals(that.msg);
    }

    @Override
    public int hashCode() {
        int result = node.hashCode();
        result = 31 * result + sender.hashCode();
        result = 31 * result + msg.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ChatMessageInfo{" +
                "node='" + node + '\'' +
                ", sender='" + sender + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
