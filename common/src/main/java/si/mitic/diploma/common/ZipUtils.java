package si.mitic.diploma.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipUtils {

    private static final Logger logger = LoggerFactory.getLogger("zip-utils");

    // Adapted from: https://stackoverflow.com/a/32052016
    public static byte[] zip(Path sourceDirPath, boolean delete) throws IOException {
        //logger.info("Zipping {}, delete: {}", sourceDirPath, delete);
        try (
                ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                ZipOutputStream zs = new ZipOutputStream(byteStream)
        ) {
            Files.walkFileTree(sourceDirPath, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Path source = sourceDirPath.relativize(file);
                    //logger.info("Adding {}", source);
                    ZipEntry zipEntry = new ZipEntry(source.toString());
                    zs.putNextEntry(zipEntry);
                    Files.copy(file, zs);
                    zs.flush();
                    zs.closeEntry();
                    if (delete) {
                        Files.delete(file);
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    if (exc == null) {
                        if (delete)
                            Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    } else throw exc;
                }
            });
            zs.finish();
            //logger.info("Finished zipping!");
            return byteStream.toByteArray();
        }
    }

    public static void unzip(Path targetDirPath, byte[] bytes) throws IOException {
        //logger.info("Unzipping to {}", targetDirPath);

        try (
                ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);
                ZipInputStream zipStream = new ZipInputStream(byteStream)
        ) {
            ZipEntry nextEntry;
            while ((nextEntry = zipStream.getNextEntry()) != null) {
                Path target = targetDirPath.resolve(nextEntry.getName());
                Files.createDirectories(target.getParent());
                //logger.info("Unzipping file {}", target);
                Files.copy(zipStream, target, StandardCopyOption.REPLACE_EXISTING);
                zipStream.closeEntry();
            }
        }
    }

}
