package si.mitic.diploma.common.data;

import java.util.Collections;

public class LobbyInfo extends ServerInfo {

    public LobbyInfo() {
        super();
    }

    public LobbyInfo(ServerData data, ServerStatus status, int playerCount) {
        super(data, status, playerCount, Collections.emptyList());
    }

}
