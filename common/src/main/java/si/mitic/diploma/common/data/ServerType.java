package si.mitic.diploma.common.data;

public enum ServerType {

    LOBBY,
    NODE,
    PROXY;

    public boolean isServer() {
        return switch (this) {
            case LOBBY, NODE -> true;
            case PROXY -> false;
        };
    }

}
