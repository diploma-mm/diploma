package si.mitic.diploma.common;

import org.redisson.api.RMapCache;
import si.mitic.diploma.common.data.WorldInfo;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class RedisWorldManager {

    private final RMapCache<UUID, WorldInfo> worldStatus;

    public RedisWorldManager(Redis redis) {
        worldStatus = redis.get().getMapCache("world-info", redis.getSerializationCodec());
    }

    public WorldInfo getWorldInfo(UUID uuid) {
        WorldInfo info = worldStatus.get(uuid);
        return info == null ? WorldInfo.DEFAULT : info;
    }

    public WorldInfo setWorldInfo(UUID uuid, WorldInfo info) {
        return worldStatus.put(uuid, info, 15, TimeUnit.SECONDS);
    }

    public WorldInfo removeWorldInfo(UUID uuid) {
        return worldStatus.remove(uuid);
    }

}
