package si.mitic.diploma.common.data;

import java.io.Serializable;

public class ServerData implements Serializable {

    public static ServerData getPodInfoFromEnv(ServerType type) {
        String maxWorlds = System.getenv("MAX_WORLDS");
        int worlds;
        if (maxWorlds == null)
            worlds = 0;
        else
            worlds = Integer.parseInt(maxWorlds);
        return new ServerData(System.getenv("KUBE_POD_NAME"), System.getenv("KUBE_POD_IP"), type, worlds);
    }

    private String name;
    private String ip;
    private ServerType type;
    private int maxWorlds;

    private ServerData() {

    }

    private ServerData(String name, String ip, ServerType type, int maxWorlds) {
        this.name = name;
        this.ip = ip;
        this.type = type;
        this.maxWorlds = maxWorlds;
    }

    public String getName() {
        return name;
    }

    public String getIp() {
        return ip;
    }

    public ServerType getType() {
        return type;
    }

    public int getMaxWorlds() {
        return maxWorlds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServerData serverData)) return false;
        return name.equals(serverData.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "PodInfo{" +
                "name='" + name + '\'' +
                ", ip='" + ip + '\'' +
                ", type=" + type +
                '}';
    }
}
