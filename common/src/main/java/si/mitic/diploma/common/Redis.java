package si.mitic.diploma.common;

import org.redisson.Redisson;
import org.redisson.api.RMapCache;
import org.redisson.api.RRemoteService;
import org.redisson.api.RTopic;
import org.redisson.api.RedissonClient;
import org.redisson.codec.SerializationCodec;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mitic.diploma.common.data.ChatMessageInfo;
import si.mitic.diploma.common.data.ServerInfo;

import java.util.function.BiConsumer;

public class Redis {

    private final SerializationCodec serializationCodec;

    public static RedisConfig getConfigFromEnv() {
        return new RedisConfig(
                System.getenv("REDIS_HOST"),
                Integer.parseInt(System.getenv("REDIS_PORT"))
        );
    }

    private final RedissonClient redisson;
    private final RedisWorldManager worldManager;
    private final RMapCache<String, ServerInfo> servers;
    private final RTopic chat;
    private final Logger logger;

    public Redis(RedisConfig conf) {
        logger = LoggerFactory.getLogger("redis");
        logger.info("Starting redis with {}!", conf);
        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + conf.hostname() + ":" + conf.port());

        redisson = Redisson.create(config);
        serializationCodec = new SerializationCodec();

        worldManager = new RedisWorldManager(this);
        servers = redisson.getMapCache("servers", serializationCodec);
        chat = redisson.getTopic("chat", serializationCodec);
    }

    public SerializationCodec getSerializationCodec() {
        return serializationCodec;
    }

    public RedissonClient get() {
        return redisson;
    }

    public RMapCache<String, ServerInfo> getServers() {
        return servers;
    }

    public RRemoteService getService(String name) {
        return redisson.getRemoteService(name, serializationCodec);
    }

    public void addChatListener(String node, BiConsumer<String, String> userAndMessageConsumer) {
        chat.addListener(ChatMessageInfo.class, (channel, msg) -> {
            if (msg.getNode().equals(node)) return;
            userAndMessageConsumer.accept(msg.getSender(), msg.getMsg());
        });
    }

    public void sendChatMessage(ChatMessageInfo info) {
        chat.publish(info);
    }

    public RedisWorldManager getWorldManager() {
        return worldManager;
    }
}
