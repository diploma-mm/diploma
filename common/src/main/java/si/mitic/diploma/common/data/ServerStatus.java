package si.mitic.diploma.common.data;

public enum ServerStatus {

    STARTING,
    RUNNING,
    STOPPING,
    STOPPED

}
