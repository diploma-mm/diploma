package si.mitic.diploma.common.data;

import java.util.List;
import java.util.UUID;

public class NodeInfo extends ServerInfo {

    public NodeInfo() {
        super();
    }

    public NodeInfo(ServerData data, ServerStatus status, int playerCount, List<UUID> worlds) {
        super(data, status, playerCount, worlds);
    }

}
