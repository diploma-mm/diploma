package si.mitic.diploma.common.data;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class ServerInfo implements Serializable {

    private ServerData data;
    private ServerStatus status;
    private int playerCount;
    private List<UUID> worlds;

    public ServerInfo() {

    }

    public ServerInfo(ServerData data, ServerStatus status, int playerCount, List<UUID> worlds) {
        this.data = data;
        this.status = status;
        this.playerCount = playerCount;
        this.worlds = worlds;
    }

    public ServerData getData() {
        return data;
    }

    public String getName() {
        return data.getName();
    }

    public String getIp() {
        return data.getIp();
    }

    public ServerType getType() {
        return data.getType();
    }

    public ServerStatus getStatus() {
        return status;
    }

    public List<UUID> getWorlds() {
        return worlds;
    }

    public int getWorldCount() {
        return worlds.size();
    }

    public int getPlayerCount() {
        return playerCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServerInfo that)) return false;
        return data.equals(that.data);
    }

    @Override
    public int hashCode() {
        return data.hashCode();
    }

    @Override
    public String toString() {
        return "ServerInfo{" +
                "info=" + data +
                ", status=" + status +
                ", playerCount=" + playerCount +
                '}';
    }
}
