package si.mitic.diploma.common;

public record RedisConfig(String hostname, int port) {

}
