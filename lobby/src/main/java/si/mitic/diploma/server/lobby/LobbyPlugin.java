package si.mitic.diploma.server.lobby;

import org.bukkit.Bukkit;
import si.mitic.diploma.common.data.LobbyInfo;
import si.mitic.diploma.common.data.ServerInfo;
import si.mitic.diploma.common.data.ServerType;
import si.mitic.diploma.server.ServerPlugin;

public class LobbyPlugin extends ServerPlugin {

    public LobbyPlugin() {
        super(ServerType.LOBBY);
    }

    @Override
    public void load() {

    }

    @Override
    public void enable() {
    }

    @Override
    public void disable() {

    }

    @Override
    public ServerInfo getInfo() {
        return new LobbyInfo(serverData, status, Bukkit.getOnlinePlayers().size());
    }

    @Override
    public void beginShutdown() {

    }

}
