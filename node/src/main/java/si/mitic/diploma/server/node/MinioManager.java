package si.mitic.diploma.server.node;

import io.minio.*;
import io.minio.errors.*;
import io.minio.messages.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class MinioManager {

    private final Logger logger;
    private final NodePlugin plugin;
    private final MinioClient minioClient;
    private final String minioWorldBucketName;

    public MinioManager(NodePlugin plugin) {
        this.logger = LoggerFactory.getLogger("MinIO Manager");
        this.plugin = plugin;

        logger.info("Setting-up MinIO manager!");

        String minioHostname = getFromEnv("MINIO_HOSTNAME");
        String minioAccessKey = getFromEnv("MINIO_ACCESS_KEY");
        String minioSecretKey = getFromEnv("MINIO_SECRET_KEY");
        minioWorldBucketName = getFromEnv("MINIO_WORLD_BUCKET_NAME");

        logger.info("Using hostname {} and bucket {}.", minioHostname, minioWorldBucketName);

        this.minioClient = MinioClient.builder()
                .endpoint(minioHostname)
                .credentials(minioAccessKey, minioSecretKey)
                .build();


        try {
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket(minioWorldBucketName).build());

            if (!found) {
                logger.info("Creating bucket {}, as it does not exist!", minioWorldBucketName);
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(minioWorldBucketName).build());
            }
        }
        catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException |
               IOException | NoSuchAlgorithmException | ServerException | XmlParserException e) {

            throw new RuntimeException(e);
        }

    }

    public void upload(String id, byte[] bytes) throws IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        try {
            logger.info("Uploading {} to the bucket!", id);
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket(minioWorldBucketName)
                            .object(id)
                            .stream(inputStream, bytes.length, -1)
                            .build()
            );
            logger.info("Successfully uploaded {} to the bucket!", id);

        }
        catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException |
               NoSuchAlgorithmException | ServerException | XmlParserException e) {
            throw new IOException(e);
        }
    }

    public List<String> list() throws IOException {
        Iterable<Result<Item>> results = minioClient.listObjects(
                ListObjectsArgs.builder()
                        .bucket(minioWorldBucketName)
                        .build()
        );
        List<String> list = new ArrayList<>();
        for (Result<Item> r : results) {
            try {
                list.add(r.get().objectName());
            }
            catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException |
                   NoSuchAlgorithmException | ServerException | XmlParserException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public boolean exists(String name) throws IOException {
        try {
            StatObjectResponse statObjectResponse = minioClient.statObject(
                    StatObjectArgs.builder().bucket(minioWorldBucketName).object(name).build()
            );
            return true;
        }
        catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException |
               NoSuchAlgorithmException | ServerException | XmlParserException e) {
            return false;
        }
    }

    public byte[] download(String name) throws IOException {
        try {
            GetObjectResponse response = minioClient.getObject(
                    GetObjectArgs.builder().bucket(minioWorldBucketName).object(name).build()
            );
            byte[] bytes = response.readAllBytes();
            response.close();
            return bytes;
        }
        catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException |
               NoSuchAlgorithmException | ServerException | XmlParserException e) {
            throw new IOException(e);
        }
    }

    public void delete(String name) throws IOException {
        try {
            minioClient.removeObject(
                    RemoveObjectArgs.builder().bucket(minioWorldBucketName).object(name).build()
            );
        }
        catch (ErrorResponseException | InsufficientDataException | InternalException | InvalidKeyException | InvalidResponseException |
               NoSuchAlgorithmException | ServerException | XmlParserException e) {
            e.printStackTrace();
        }
    }

    private String getFromEnv(String name) {
        String param = System.getenv(name);
        if (param == null)
            throw new RuntimeException("Error loading MinIO: " + name + " is not defined!");
        return param;
    }

}
