package si.mitic.diploma.server.node;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.util.TriState;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mitic.diploma.common.RedisWorldManager;
import si.mitic.diploma.common.ZipUtils;
import si.mitic.diploma.common.data.WorldInfo;
import si.mitic.diploma.common.data.WorldStatus;
import si.mitic.diploma.common.interfaces.NodeService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class WorldManager implements NodeService {

    private final ConcurrentHashMap<UUID, WorldStatus> localWorldStatus;
    private final NodePlugin plugin;
    private final Logger logger;
    private final MinioManager minioManager;
    private final RedisWorldManager worldManager;

    public WorldManager(NodePlugin plugin, MinioManager minioManager) {
        this.plugin = plugin;
        this.minioManager = minioManager;
        this.localWorldStatus = new ConcurrentHashMap<>();
        this.worldManager = plugin.getRedis().getWorldManager();
        this.logger = LoggerFactory.getLogger("world-manager");
        Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () -> {
            Iterator<Map.Entry<UUID, WorldStatus>> iterator = localWorldStatus.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<UUID, WorldStatus> e = iterator.next();
                UUID uuid = e.getKey();
                WorldStatus status = e.getValue();
                if (status == WorldStatus.UNASSIGNED) {
                    iterator.remove();
                    worldManager.removeWorldInfo(uuid);
                } else {
                    WorldInfo oldInfo = worldManager.setWorldInfo(uuid, new WorldInfo(status, plugin.getServerData().getName()));
                    if (oldInfo != null && !status.equals(oldInfo.getStatus())) {
                        logger.info("Changing status of {} from {} to {}!", uuid, oldInfo, status);
                    }
                }
            }
        }, 10, 10);
    }

    @Override
    public boolean prepareWorld(UUID playerUUID, UUID worldUUID) {
        if (isWorldLoaded(worldUUID))
            return true;
        if (getLoadedWorlds().size() >= plugin.getServerData().getMaxWorlds())
            return false;

        String worldName = worldUUID.toString();
        if (!playerUUID.equals(worldUUID) && !worldExists(worldUUID))
            return false;

        logger.info("Loading world for {}", worldName);
        localWorldStatus.put(worldUUID, WorldStatus.LOADING);

        try {
            if (minioManager.exists(worldName)) {
                byte[] bytes = minioManager.download(worldName);
                Path targetFolder = Bukkit.getWorldContainer().toPath().resolve(worldName);
                if (Files.notExists(targetFolder)) {
                    Files.createDirectory(targetFolder);
                }
                ZipUtils.unzip(targetFolder, bytes);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            localWorldStatus.put(worldUUID, WorldStatus.UNASSIGNED);
            return false;
        }

        plugin.getPlayerToWorldMap().put(playerUUID, worldUUID);

        loadOrCreateWorld(worldUUID);
        return true;
    }

    public boolean worldExists(UUID uuid) {
        try {
            return minioManager.exists(uuid.toString());
        }
        catch (IOException e) {
            return false;
        }
    }

    @Override
    public void setPlayerVisiting(UUID player, UUID world) {
        plugin.getPlayerToWorldMap().put(player, world);
    }

    public boolean isWorldLoaded(UUID uuid) {
        WorldStatus worldStatus = localWorldStatus.getOrDefault(uuid, WorldStatus.UNASSIGNED);
        logger.info("Checking world load! {} - {}", uuid, worldStatus);
        return worldStatus == WorldStatus.READY;
    }

    public Optional<World> getWorld(UUID uuid) {
        return Optional.ofNullable(Bukkit.getWorld(uuid.toString()));
    }

    public List<UUID> getLoadedWorlds() {
        return localWorldStatus.entrySet().stream()
                .filter(e -> !e.getValue().equals(WorldStatus.UNASSIGNED))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    private void loadOrCreateWorld(UUID uuid) {
        Bukkit.getScheduler().runTask(plugin, () -> {
            String worldName = uuid.toString();
            try {
                logger.info("Creating world for {}", uuid);
                //TODO Make sure this works
                WorldCreator creator = new WorldCreator(worldName);
                creator.keepSpawnLoaded(TriState.FALSE);
                creator.type(WorldType.FLAT);
                World world = creator.createWorld();
                if (world == null) {
                    localWorldStatus.put(uuid, WorldStatus.UNASSIGNED);
                    logger.warn("Got null World when creating for {}.", uuid);
                    return;
                }
                world.setDifficulty(Difficulty.NORMAL);
                world.setViewDistance(5);
                world.setSimulationDistance(5);
                world.setSendViewDistance(5);
                logger.info("Created world for {}", uuid);
                localWorldStatus.put(uuid, WorldStatus.READY);
            }
            catch (Exception e) {
                localWorldStatus.put(uuid, WorldStatus.UNASSIGNED);
                logger.warn("Got error when creating world for {}.", uuid);
                logger.warn("Error:", e);
            }
        });
    }

    public void saveWorld(UUID uuid) {
        Optional<World> worldOptional = getWorld(uuid);
        if (worldOptional.isEmpty()) return;

        World world = worldOptional.get();
        logger.info("Saving world for {}", uuid);
        localWorldStatus.put(uuid, WorldStatus.SAVING);

        long currentlySaving = localWorldStatus.values().stream().filter(i -> i.equals(WorldStatus.SAVING)).count();

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            for (Player p : world.getPlayers()) {
                p.kick(Component.text("World unloading!", NamedTextColor.RED));
            }
            Path worldFolder = world.getWorldFolder().toPath();
            Bukkit.unloadWorld(world, true);
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                try {
                    byte[] bytes = ZipUtils.zip(worldFolder, false);
                    minioManager.upload(uuid.toString(), bytes);
                    localWorldStatus.put(uuid, WorldStatus.UNASSIGNED);
                    logger.info("Saved world for {}", uuid);
                }
                catch (IOException e) {
                    localWorldStatus.put(uuid, WorldStatus.UNASSIGNED);
                    logger.warn("Error saving world for {}", uuid);
                    logger.warn("Error: ", e);
                }
            });
        }, 20 * currentlySaving);
    }

}
