package si.mitic.diploma.server.node;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Optional;
import java.util.UUID;

public class NodeListener implements Listener {

    private final NodePlugin plugin;

    public NodeListener(NodePlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerLoginEvent event) {
        if (event.getPlayer().getName().equals("Red_Epicness")) return;
        UUID playerUUID = event.getPlayer().getUniqueId();
        UUID worldUUID = plugin.getPlayerSelectedWorld(playerUUID);
        boolean worldLoaded = plugin.getWorldManager().isWorldLoaded(worldUUID);
        if (!worldLoaded) {
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, Component.text("The world is not ready!"));
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (event.getPlayer().getName().equals("Red_Epicness")) return;
        Player p = event.getPlayer();
        UUID playerUUID = p.getUniqueId();
        UUID worldUUID = plugin.getPlayerSelectedWorld(playerUUID);
        Optional<World> world = plugin.getWorldManager().getWorld(worldUUID);
        if (world.isEmpty()) {
            p.kick(Component.text("Error loading world!", NamedTextColor.RED));
            return;
        }
        if (playerUUID.equals(worldUUID)) {
            p.setGameMode(GameMode.CREATIVE);
        } else {
            p.setGameMode(GameMode.ADVENTURE);
        }
        p.teleport(world.get().getSpawnLocation());
    }

    @EventHandler
    public void onJoin(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        UUID playerUUID = player.getUniqueId();
        try {
            UUID playerSelectedWorld = plugin.getPlayerSelectedWorld(playerUUID);
            Optional<World> world = plugin.getWorldManager().getWorld(playerSelectedWorld);
            if (world.isEmpty()) return;
            if (world.get().getPlayers().size() <= 1) {
                plugin.getWorldManager().saveWorld(playerSelectedWorld);
            }
        }
        catch (Exception ignored) {
        }
        plugin.getPlayerToWorldMap().remove(playerUUID);
    }

}
