package si.mitic.diploma.server.node;

import org.bukkit.Bukkit;
import si.mitic.diploma.common.data.NodeInfo;
import si.mitic.diploma.common.data.ServerInfo;
import si.mitic.diploma.common.data.ServerType;
import si.mitic.diploma.common.interfaces.NodeService;
import si.mitic.diploma.server.ServerPlugin;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;

public class NodePlugin extends ServerPlugin {

    private WorldManager worldManager;
    private MinioManager minioManager;
    private final HashMap<UUID, UUID> playerToWorldMap = new HashMap<>();

    public NodePlugin() {
        super(ServerType.NODE);
    }

    @Override
    public void load() {
        minioManager = new MinioManager(this);
    }

    @Override
    public void enable() {

        worldManager = new WorldManager(this, minioManager);
        redis.getService(serverData.getName()).register(NodeService.class, worldManager, 5);

        Bukkit.getPluginManager().registerEvents(new NodeListener(this), this);
        Bukkit.getScheduler().runTaskTimer(this, () -> {
            double[] tps = Bukkit.getTPS();
            getSLF4JLogger().info("[SERVER-TICK] {},{},{},{},{}", Instant.now().getEpochSecond(), Bukkit.getOnlinePlayers().size(), String.format("%.2f", tps[0]), String.format("%.2f", tps[1]), String.format("%.2f", tps[2]));
        }, 20, 20);
    }

    @Override
    public void disable() {

    }

    @Override
    public void beginShutdown() {

    }

    public HashMap<UUID, UUID> getPlayerToWorldMap() {
        return playerToWorldMap;
    }

    public UUID getPlayerSelectedWorld(UUID uuid) {
        return playerToWorldMap.getOrDefault(uuid, uuid);
    }

    @Override
    public ServerInfo getInfo() {
        return new NodeInfo(serverData, status, Bukkit.getOnlinePlayers().size(), worldManager == null ? Collections.emptyList() : worldManager.getLoadedWorlds());
    }

    public WorldManager getWorldManager() {
        return worldManager;
    }
}
