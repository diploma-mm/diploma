package si.mitic.diploma.server.singlenode;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.util.TriState;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mitic.diploma.common.data.WorldStatus;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class WorldManager {

    private final ConcurrentHashMap<UUID, WorldStatus> localWorldStatus;
    private final SingleNodePlugin plugin;
    private final Logger logger;

    public WorldManager(SingleNodePlugin plugin) {
        this.plugin = plugin;
        this.localWorldStatus = new ConcurrentHashMap<>();
        this.logger = LoggerFactory.getLogger("world-manager");
        Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () -> {
            Iterator<Map.Entry<UUID, WorldStatus>> iterator = localWorldStatus.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<UUID, WorldStatus> e = iterator.next();
                WorldStatus status = e.getValue();
                if (status == WorldStatus.UNASSIGNED) {
                    iterator.remove();
                }
            }
        }, 10, 10);
    }

    public boolean isWorldLoaded(UUID uuid) {
        WorldStatus worldStatus = localWorldStatus.getOrDefault(uuid, WorldStatus.UNASSIGNED);
        logger.info("Checking world load! {} - {}", uuid, worldStatus);
        return worldStatus == WorldStatus.READY;
    }

    public Optional<World> getWorld(UUID uuid) {
        return Optional.ofNullable(Bukkit.getWorld(uuid.toString()));
    }

    public List<UUID> getLoadedWorlds() {
        return localWorldStatus.entrySet().stream()
                .filter(e -> !e.getValue().equals(WorldStatus.UNASSIGNED))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public void loadOrCreateWorld(UUID uuid) {
        String worldName = uuid.toString();
        logger.info("Loading world for {}", worldName);
        localWorldStatus.put(uuid, WorldStatus.LOADING);
        try {
            logger.info("Creating world for {}", uuid);
            //TODO Make sure this works
            WorldCreator creator = new WorldCreator(worldName);
            creator.keepSpawnLoaded(TriState.FALSE);
            creator.type(WorldType.FLAT);
            World world = creator.createWorld();
            if (world == null) {
                localWorldStatus.put(uuid, WorldStatus.UNASSIGNED);
                logger.warn("Got null World when creating for {}.", uuid);
                return;
            }
            world.setDifficulty(Difficulty.NORMAL);
            world.setViewDistance(5);
            world.setSimulationDistance(5);
            world.setSendViewDistance(5);
            logger.info("Created world for {}", uuid);
            localWorldStatus.put(uuid, WorldStatus.READY);
        }
        catch (Exception e) {
            localWorldStatus.put(uuid, WorldStatus.UNASSIGNED);
            logger.warn("Got error when creating world for {}.", uuid);
            logger.warn("Error:", e);
        }
    }

    public void saveWorld(UUID uuid) {
        Optional<World> worldOptional = getWorld(uuid);
        if (worldOptional.isEmpty()) return;

        World world = worldOptional.get();
        logger.info("Saving world for {}", uuid);
        localWorldStatus.put(uuid, WorldStatus.SAVING);

        long currentlySaving = localWorldStatus.values().stream().filter(i -> i.equals(WorldStatus.SAVING)).count();

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            for (Player p : world.getPlayers()) {
                p.kick(Component.text("World unloading!", NamedTextColor.RED));
            }
            Bukkit.unloadWorld(world, true);
            localWorldStatus.put(uuid, WorldStatus.UNASSIGNED);
            logger.info("Saved world for {}", uuid);
        }, 20 * currentlySaving);
    }

}
