package si.mitic.diploma.server.singlenode;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class SingleNodePlugin extends JavaPlugin {

    private WorldManager worldManager;

    @Override
    public void onLoad() {
    }

    @Override
    public void onEnable() {

        worldManager = new WorldManager(this);

        Bukkit.getPluginManager().registerEvents(new NodeListener(this), this);
        Bukkit.getScheduler().runTaskTimer(this, () -> {
            double[] tps = Bukkit.getTPS();
            getSLF4JLogger().info("[SERVER-TICK] {},{},{},{}", Bukkit.getOnlinePlayers().size(), tps[0], tps[1], tps[2]);
        }, 20, 20);
    }

    public WorldManager getWorldManager() {
        return worldManager;
    }
}
