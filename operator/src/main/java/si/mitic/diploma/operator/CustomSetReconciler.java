package si.mitic.diploma.operator;

import io.fabric8.kubernetes.api.model.Pod;
import io.javaoperatorsdk.operator.api.reconciler.Context;
import io.javaoperatorsdk.operator.api.reconciler.ControllerConfiguration;
import io.javaoperatorsdk.operator.api.reconciler.Reconciler;
import io.javaoperatorsdk.operator.api.reconciler.UpdateControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mitic.diploma.operator.crds.CustomSet;
import si.mitic.diploma.operator.crds.CustomSetStatus;

import java.util.List;

@ControllerConfiguration()
public class CustomSetReconciler implements Reconciler<CustomSet> {

    private final KubernetesHelper kubernetesHelper;
    private final Logger logger;

    public CustomSetReconciler(KubernetesHelper kubernetesHelper) {
        this.kubernetesHelper = kubernetesHelper;
        this.logger = LoggerFactory.getLogger("Set Reconciler");
    }

    @Override
    public UpdateControl<CustomSet> reconcile(CustomSet set, Context<CustomSet> context) {
        try {
            String name = set.getMetadata().getName();
            logger.info("Reconciling set {}.", name);

            List<Pod> nonTerminatingPods = kubernetesHelper.getNonTerminatingPods(set);
            int activePods = nonTerminatingPods.size();
            int minReplicas = set.getSpec().getMinReplicas();

            logger.info("Set has {} child pods, target is {}.", activePods, minReplicas);

            if (activePods >= minReplicas) {
                logger.info("No update required!");
                return UpdateControl.noUpdate();
            }

            if (set.getStatus() == null) {
                set.setStatus(new CustomSetStatus());
            }

            int missingPods = minReplicas - activePods;
            for (int i = 0; i < missingPods; i++) {
                kubernetesHelper.deployPod(set, false);
            }
            logger.info("Updating set status!");
            return UpdateControl.updateStatus(set);
        }
        catch (Exception e) {
            e.printStackTrace();
            return UpdateControl.noUpdate();
        }
    }


}
