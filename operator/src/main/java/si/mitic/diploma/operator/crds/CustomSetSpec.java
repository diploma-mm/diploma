package si.mitic.diploma.operator.crds;

import io.fabric8.kubernetes.api.model.PodTemplateSpec;
import si.mitic.diploma.common.data.ServerType;

public class CustomSetSpec {
    private int minReplicas;
    private int maxPlayers;
    private ServerType type;
    private PodTemplateSpec template;

    public CustomSetSpec() {
    }

    public CustomSetSpec(int minReplicas, PodTemplateSpec template) {
        this.minReplicas = minReplicas;
        this.template = template;
    }

    public int getMinReplicas() {
        return minReplicas;
    }

    public void setMinReplicas(int minReplicas) {
        this.minReplicas = minReplicas;
    }

    public ServerType getType() {
        return type;
    }

    public void setType(ServerType type) {
        this.type = type;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public PodTemplateSpec getTemplate() {
        return template;
    }

    public void setTemplate(PodTemplateSpec template) {
        this.template = template;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomSetSpec that)) return false;

        if (minReplicas != that.minReplicas) return false;
        return template != null ? template.equals(that.template) : that.template == null;
    }

    @Override
    public int hashCode() {
        int result = minReplicas;
        result = 31 * result + (template != null ? template.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CustomSetSpec{" +
                "minReplicas=" + minReplicas +
                ", template=" + template +
                '}';
    }
}
