package si.mitic.diploma.operator;

import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.javaoperatorsdk.operator.Operator;
import si.mitic.diploma.common.Redis;

import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        Config config = new ConfigBuilder().withNamespace("default").build();
        DefaultKubernetesClient kubernetesClient = new DefaultKubernetesClient(config);
        KubernetesHelper kubernetesHelper = new KubernetesHelper(kubernetesClient);
        Redis redis = new Redis(Redis.getConfigFromEnv());
        Timer timer = new Timer();
        ExecutorService statusUpdaterExecutor = Executors.newSingleThreadExecutor();

        String namespace = System.getenv("NAMESPACE");
        String setNamesRaw = System.getenv("SET_NAMES");
        String[] setNames;

        if (namespace == null) namespace = "default";
        if (setNamesRaw == null) setNames = new String[]{};
        else setNames = setNamesRaw.split(",");

        long updatePeriod = TimeUnit.SECONDS.toMillis(10);
        long delay = 0;

        for (String setName : setNames) {
            timer.scheduleAtFixedRate(
                    new SetUpdater(kubernetesHelper, namespace, setName, redis),
                    delay,
                    updatePeriod
            );
            delay += 500; // Offset the updaters a bit
        }

        statusUpdaterExecutor.submit(new StatusUpdater(namespace, kubernetesClient, kubernetesHelper));

        Operator operator = new Operator(kubernetesClient);
        operator.register(new CustomSetReconciler(kubernetesHelper));
        operator.start();
    }

}
