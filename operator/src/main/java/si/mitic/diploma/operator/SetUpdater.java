package si.mitic.diploma.operator;

import io.fabric8.kubernetes.api.model.Pod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mitic.diploma.common.Redis;
import si.mitic.diploma.common.data.ServerInfo;
import si.mitic.diploma.common.data.ServerType;
import si.mitic.diploma.operator.crds.CustomSet;

import java.time.LocalDateTime;
import java.util.*;

public class SetUpdater extends TimerTask {

    private final Logger logger;
    private final KubernetesHelper kubernetesHelper;
    private final Redis redis;
    private final String namespace;
    private final String setName;

    private SetInfo setInfo;

    protected SetUpdater(KubernetesHelper kubernetesHelper, String namespace, String setName, Redis redis) {
        this.redis = redis;
        this.logger = LoggerFactory.getLogger("Set Updater - " + namespace + "/" + setName);
        this.kubernetesHelper = kubernetesHelper;
        this.namespace = namespace;
        this.setName = setName;
        logger.info("Created new set updater!");
    }

    public final void run() {
        try {
            CustomSet customSet = kubernetesHelper.getCustomSet(namespace, setName);
            if (customSet == null) {
                logger.warn("Could not find set {}/{}, skipping update!", namespace, setName);
                return;
            }
            this.setInfo = new SetInfo(customSet);
            List<PodInfo> pods = kubernetesHelper.getNonTerminatingPods(setInfo.set).stream().map(PodInfo::new).toList();

            int minReplicas = setInfo.getMinReplicas();
            long generation = setInfo.getGeneration();
            int podCount = pods.size();

            logger.info("Updating the set with {} pods. (min-replicas: {}, generation: {})", podCount, minReplicas, generation);

            Optional<PodInfo> nonReadyPod = pods.stream().filter(pod -> !pod.isReady()).findFirst();

            if (nonReadyPod.isPresent()) {
                logger.info("Skipping update as we found a non-ready pod: {}", nonReadyPod.get().getName());
                return;
            }

            boolean notEnoughPods = podCount < minReplicas;

            if (notEnoughPods) {
                logger.info("Set has less that the minimum amount of pods! (Current: {} < Required: {})", podCount, minReplicas);
                kubernetesHelper.deployPod(setInfo.set, true);
                return;
            }


            double percentageFull;
            int capacity, maxCapacity;

            if (setInfo.getType().equals(ServerType.NODE)) {
                capacity = pods.stream().mapToInt(PodInfo::getWorldCount).sum();
                maxCapacity = pods.size() * setInfo.getMaxPlayers();
            } else {
                capacity = pods.stream().mapToInt(PodInfo::getPlayerCount).sum();
                maxCapacity = pods.size() * setInfo.getMaxPlayers();
            }
            percentageFull = maxCapacity == 0 ? 1 : capacity / (float) maxCapacity;

            if (percentageFull > 0.8) {
                logger.info("Set does not have enough pods to support demand! Capacity: {}/{} ({}%)", capacity, maxCapacity, percentageFull * 100);
                kubernetesHelper.deployPod(setInfo.set, true);
                return;
            }

            boolean hasMoreThanMinimumServers = podCount > minReplicas;

            if (percentageFull < 0.5 && hasMoreThanMinimumServers) {
                logger.info("Set has too many pods, based on demand! Capacity: {}/{} ({}%)", capacity, maxCapacity, percentageFull * 100);
                PodInfo minPlayers = Collections.min(pods, Comparator.comparing(PodInfo::getPlayerCount));
                kubernetesHelper.deletePod(minPlayers.pod, false);
                return;
            }

            Optional<PodInfo> outdatedServer = pods.stream().filter(PodInfo::isDifferentGeneration).findAny();
            if (outdatedServer.isPresent()) {
                PodInfo podInfo = outdatedServer.get();
                logger.info("Found outdated pod with generation {}! (Current generation: {})", podInfo.getGeneration(), generation);
                if (podCount - 1 < minReplicas) {
                    logger.info("Deploying new pod, because deleting the outdated pod would drop the number of pods below the minimum required!");
                    kubernetesHelper.deployPod(setInfo.set, true);
                    return;
                }
                kubernetesHelper.deletePod(podInfo.pod, false);
                return;
            }

            logger.info("No update required!");
        }
        catch (Exception e) {
            logger.warn("Error updating set!", e);
        }

    }

    public record SetInfo(CustomSet set) {

        public String getName() {
            return set.getMetadata().getName();
        }

        public String getNamespace() {
            return set.getMetadata().getNamespace();
        }

        public long getGeneration() {
            return set.getMetadata().getGeneration();
        }

        public int getMinReplicas() {
            return set.getSpec().getMinReplicas();
        }

        public ServerType getType() {
            return set.getSpec().getType();
        }

        public int getMaxPlayers() {
            return set.getSpec().getMaxPlayers();
        }

    }

    public class PodInfo {

        private final Pod pod;
        private Integer playerCount = null;

        public PodInfo(Pod pod) {
            this.pod = pod;
        }

        public Pod getPod() {
            return pod;
        }

        public String getName() {
            return pod.getMetadata().getName();
        }

        public String getNamespace() {
            return pod.getMetadata().getNamespace();
        }

        public int getPlayerCount() {
            if (playerCount == null)
                playerCount = Optional.ofNullable(redis.getServers().get(getName()))
                        .map(ServerInfo::getPlayerCount)
                        .orElse(0);
            return playerCount;
        }

        public int getWorldCount() {
            if (playerCount == null)
                playerCount = Optional.ofNullable(redis.getServers().get(getName()))
                        .map(ServerInfo::getWorldCount)
                        .orElse(0);
            return playerCount;
        }

        public boolean isDifferentGeneration() {
            return getGeneration() != setInfo.getGeneration();
        }

        public long getGeneration() {
            return Long.parseLong(pod.getMetadata().getLabels().get("operator-generation"));
        }

        public LocalDateTime getCreatedDateTime() {
            return LocalDateTime.parse(pod.getMetadata().getCreationTimestamp());
        }

        public boolean isReady() {
            return pod.getStatus().getConditions().stream()
                    .filter(el -> el.getType().equals("Ready"))
                    .findFirst()
                    .map(el -> Boolean.valueOf(el.getStatus()))
                    .orElse(false);
        }

    }

}
