package si.mitic.diploma.operator.crds;

import io.fabric8.kubernetes.api.model.Namespaced;
import io.fabric8.kubernetes.client.CustomResource;
import io.fabric8.kubernetes.model.annotation.Group;
import io.fabric8.kubernetes.model.annotation.Version;

@Group("mitic.si")
@Version("v1alpha1")
public class CustomSet extends CustomResource<CustomSetSpec, CustomSetStatus> implements Namespaced {

    @Override
    protected CustomSetStatus initStatus() {
        return new CustomSetStatus();
    }

    @Override
    public String toString() {
        return "CustomSet{" +
                "spec=" + spec +
                ", status=" + status +
                '}';
    }

}
