package si.mitic.diploma.operator.crds;

import java.util.ArrayList;
import java.util.List;

public class CustomSetStatus {
    private List<String> pods = new ArrayList<>();
    private List<String> names = new ArrayList<>();

    public CustomSetStatus() {
    }

    public CustomSetStatus(List<String> pods, List<String> names) {
        this.pods = pods;
        this.names = names;
    }

    public List<String> getPods() {
        return pods;
    }

    public void setPods(List<String> pods) {
        this.pods = pods;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomSetStatus that)) return false;

        if (pods != null ? !pods.equals(that.pods) : that.pods != null) return false;
        return names != null ? names.equals(that.names) : that.names == null;
    }

    @Override
    public int hashCode() {
        int result = pods != null ? pods.hashCode() : 0;
        result = 31 * result + (names != null ? names.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "si.mitic.diploma.operator.crds.MinecraftSetStatus{" +
                "pods=" + pods +
                ", names=" + names +
                '}';
    }
}
