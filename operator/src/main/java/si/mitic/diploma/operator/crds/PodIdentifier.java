package si.mitic.diploma.operator.crds;

public class PodIdentifier {
    private final String name;
    private final int index;

    public PodIdentifier(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "PodIdentifier{" +
                "name='" + name + '\'' +
                ", index=" + index +
                '}';
    }

}
