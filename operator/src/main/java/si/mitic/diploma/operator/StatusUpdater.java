package si.mitic.diploma.operator;

import io.fabric8.kubernetes.api.model.OwnerReference;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.Watcher;
import io.fabric8.kubernetes.client.WatcherException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mitic.diploma.operator.crds.CustomSet;

import java.util.Optional;

public class StatusUpdater implements Runnable, Watcher<Pod> {

    private final Logger logger;
    private final KubernetesClient kubernetesClient;
    private final KubernetesHelper kubernetesHelper;
    private final String namespace;


    public StatusUpdater(String namespace, KubernetesClient kubernetesClient, KubernetesHelper kubernetesHelper) {
        this.namespace = namespace;
        this.logger = LoggerFactory.getLogger("Status Updater");
        this.kubernetesClient = kubernetesClient;
        this.kubernetesHelper = kubernetesHelper;
        logger.info("Created the status updater!");
    }

    @Override
    public void run() {
        logger.info("Starting status updater!");
        try {
            kubernetesClient.pods().inNamespace(namespace).watch(this);
        }
        catch (Exception e) {
            logger.error("Error during watcher: ", e);
        }
    }

    @Override
    public void eventReceived(Action action, Pod resource) {
        try {
            Optional<OwnerReference> reference = resource.getMetadata()
                    .getOwnerReferences()
                    .stream()
                    .filter(el -> el.getApiVersion().equals("mitic.si/v1alpha1") && el.getKind().equals("CustomSet"))
                    .findFirst();
            if (reference.isEmpty()) return;
            logger.info("Event: {} on {} with owner reference: {}", action, resource.getMetadata().getName(), reference.get().getName());

            boolean terminated = action == Action.MODIFIED && resource.getMetadata().getDeletionTimestamp() != null;
            boolean succeeded = action == Action.MODIFIED && resource.getStatus().getPhase().equals("Succeeded");
            boolean deleted = action == Action.DELETED;

            CustomSet set = kubernetesHelper.getCustomSet(resource.getMetadata().getNamespace(), reference.get().getName());

            if (set != null) {
                if (succeeded || terminated) {
                    logger.info("Controlled pod {}, updating parent!", succeeded ? "completed" : "terminated");
                    set.getStatus().getPods().remove(resource.getMetadata().getName());
                    kubernetesHelper.updateStatus(set);
                } else if (deleted) {
                    logger.info("Controlled pod deleted, updating parent!");
                    set.getStatus().getNames().remove(resource.getMetadata().getName());
                    kubernetesHelper.updateStatus(set);
                }
            } else {
                logger.info("Unable to find parent!");
            }

            if (succeeded) {
                kubernetesClient.pods().inNamespace(resource.getMetadata().getNamespace()).delete(resource);
            }
        }
        catch (Exception e) {
            logger.warn("Error handling event: ", e);
        }
    }

    @Override
    public void onClose(WatcherException cause) {
        logger.error("Pod watcher closed!", cause);
    }

}
