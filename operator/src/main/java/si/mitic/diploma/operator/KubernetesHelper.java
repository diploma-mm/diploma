package si.mitic.diploma.operator;

import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.api.model.PodBuilder;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import si.mitic.diploma.operator.crds.CustomSet;
import si.mitic.diploma.operator.crds.CustomSetList;
import si.mitic.diploma.operator.crds.PodIdentifier;

import java.util.List;
import java.util.stream.Collectors;

public class KubernetesHelper {
    private final KubernetesClient kubernetesClient;
    private final Logger logger;

    public KubernetesHelper(KubernetesClient kubernetesClient) {
        this.logger = LoggerFactory.getLogger("Kubernetes Helper");
        this.kubernetesClient = kubernetesClient;
    }

    public CustomSet getCustomSet(String namespace, String name) {
        return customSetOperations().inNamespace(namespace).withName(name).get();
    }

    public FilterWatchListDeletable<Pod, PodList> getPods(CustomSet resource) {
        return kubernetesClient.pods()
                .inNamespace(resource.getMetadata().getNamespace())
                .withLabel("operator-name", resource.getMetadata().getName());
    }

    public List<Pod> getNonTerminatingPods(CustomSet resource) {
        return getPods(resource).list().getItems().stream()
                .filter(el -> el.getStatus().getPhase().equals("Running") || el.getStatus().getPhase().equals("Pending"))
                .filter(el -> el.getMetadata().getDeletionTimestamp() == null)
                .collect(Collectors.toList());
    }

    public Pod deployPod(CustomSet set, boolean updateResource) {
        logger.info("Deploying new pod to set {}!", set.getMetadata().getName());
        Pod pod = createPod(set);
        set.getStatus().getPods().add(pod.getMetadata().getName());
        set.getStatus().getNames().add(pod.getMetadata().getName());
        if (updateResource) {
            updateStatus(set);
        }
        logger.info("Pod deployed!");
        return pod;
    }

    public void updateStatus(CustomSet set) {
        logger.info("Updating status of {}!", set.getMetadata().getName());
        customSetOperations().inNamespace(set.getMetadata().getNamespace()).replaceStatus(set);
    }

    private Pod createPod(CustomSet resource) {
        PodIdentifier podIdentifier = generatePodName(resource);
        logger.info("Creating pod {}!", podIdentifier.getName());
        Pod template = new PodBuilder()
                .withNewMetadataLike(new ObjectMeta())
                .addNewOwnerReference()
                .withApiVersion(resource.getApiVersion())
                .withKind(resource.getKind())
                .withController(true)
                .withName(resource.getMetadata().getName())
                .withUid(resource.getMetadata().getUid())
                .withBlockOwnerDeletion(true)
                .endOwnerReference()
                .addToLabels("operator-name", resource.getMetadata().getName())
                .addToLabels("operator-generation", String.valueOf(resource.getMetadata().getGeneration()))
                .addToLabels("app", resource.getSpec().getTemplate().getMetadata().getLabels().getOrDefault("app", "unknown"))
                .withName(podIdentifier.getName())
                .endMetadata()
                .withNewSpecLike(resource.getSpec().getTemplate().getSpec())
                .endSpec()
                .build();

        Pod pod = kubernetesClient.pods().inNamespace(resource.getMetadata().getNamespace()).createOrReplace(template);
        logger.info("Pod {} created successfully!", pod.getMetadata().getName());
        return pod;
    }

    private PodIdentifier generatePodName(CustomSet resource) {
        for (int i = 1; i < 100; i++) {
            String newName = String.format("%s-%02d", resource.getMetadata().getName(), i);

            if (resource.getStatus().getNames().contains(newName))
                continue;

            return new PodIdentifier(newName, i);

        }
        throw new RuntimeException("Unable to find a free name");
    }

    public MixedOperation<CustomSet, CustomSetList, Resource<CustomSet>> customSetOperations() {
        return kubernetesClient.resources(CustomSet.class, CustomSetList.class);
    }

    public void deletePod(Pod pod, boolean force) {
        String name = pod.getMetadata().getName();
        logger.info("Deleting pod {}! Forced: {}", name, force);
        NonNamespaceOperation<Pod, PodList, PodResource<Pod>> operation = kubernetesClient.pods().inNamespace(pod.getMetadata().getNamespace());
        if (force) {
            operation.withName(name).withGracePeriod(0).delete();
        } else {
            operation.delete(pod);
        }
        logger.info("Deleted pod {}!", name);
    }

}
